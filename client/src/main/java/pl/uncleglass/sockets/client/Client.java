package pl.uncleglass.sockets.client;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;


    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public void run() throws IOException {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            Command command = new Gson().fromJson(in.readLine(), Command.class);
            System.out.println(command.getBody());
            if (command.isEndConnection()) {
                stopConnection();
                return;
            }
            if (command.isResponseRequired()) {
                String userInput = scanner.nextLine();
                out.println(userInput);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.startConnection("127.0.0.1", 6666);
        client.run();
    }
}
