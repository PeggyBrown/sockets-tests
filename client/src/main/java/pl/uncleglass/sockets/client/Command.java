package pl.uncleglass.sockets.client;

public class Command {
    private final boolean responseRequired;
    private final String body;
    private final boolean endConnection;

    public Command(boolean responseRequired, String body, boolean endConnection) {
        this.responseRequired = responseRequired;
        this.body = body;
        this.endConnection = endConnection;
    }

    public boolean isResponseRequired() {
        return responseRequired;
    }

    public String getBody() {
        return body;
    }

    public boolean isEndConnection() {
        return endConnection;
    }
}
