package pl.uncleglass.sockets.server.user;

import java.util.LinkedList;
import java.util.Queue;

public class User {
    private static final int QUEUE_CAPACITY = 5;
    private Integer id;
    private final String name;
    private final String password;
    private final Role role;
    private  Queue<Message> messages = new LinkedList<>();

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User(int id, String name, String password, Role role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public Queue<Message> getMessages() {
        return messages;
    }

    public void setMessages(Queue<Message> messages) {
        this.messages = messages;
    }

    public boolean isAdmin() {
        return role == Role.ADMIN;
    }

    public void addMessage(Message message) {
        if (isAdmin()) {
            messages.offer(message);
        } else if (messages.size() < QUEUE_CAPACITY) {
            messages.offer(message);
        } else {
            throw new IllegalStateException("Sending the message failed. User's inbox is full.");
        }
    }

    public Message getMessage() {
        return messages.poll();
    }
}
