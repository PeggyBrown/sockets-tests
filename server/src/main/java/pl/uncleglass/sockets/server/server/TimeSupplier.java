package pl.uncleglass.sockets.server.server;

import java.time.Instant;

class TimeSupplier {
    public Instant get() {
        return Instant.now();
    }
}
