package pl.uncleglass.sockets.server.command;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import pl.uncleglass.sockets.server.db.DataManager;
import pl.uncleglass.sockets.server.server.Server;
import pl.uncleglass.sockets.server.server.UpTime;
import pl.uncleglass.sockets.server.user.Message;
import pl.uncleglass.sockets.server.user.UserManager;

import javax.security.auth.login.LoginException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class CommandManager {
    private final Server server;
    private final PrintWriter out;
    private final BufferedReader in;
    private final UserManager userManager;

    public CommandManager(Server server) throws IOException {
        this.server = server;
        out = new PrintWriter(this.server.getClientSocket().getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(this.server.getClientSocket().getInputStream()));
        userManager = new UserManager(new DataManager());
    }

    public void sendCommand(boolean responseRequired, String body) {
        Command command = new Command(responseRequired, body);
        out.println(new Gson().toJson(command));
    }

    public void sendCommand(boolean responseRequired, String body, boolean endConnection) {
        Command command = new Command(responseRequired, body, endConnection);
        out.println(new Gson().toJson(command));
    }

    public void run() throws IOException {
        while (true) {
            loginOrRegister();
            if (mainMenu()) {
                return;
            }
        }
    }

    private void loginOrRegister() throws IOException {
        if (userManager.isNoUserLoggedIn()) {
            boolean run = true;
            while (run) {
                sendCommand(true, "Choose command:\n1 - Login\n2 - Register");
                switch (in.readLine()) {
                    case "1":
                        run = !login();
                        break;
                    case "2":
                        run = !register();
                        break;
                    default:
                        sendCommand(false, "Unknown command");
                }
            }
        }
    }

    private boolean login() throws IOException {
        sendCommand(true, "Enter your username: ");
        String username = in.readLine();
        sendCommand(true, "Enter your password: ");
        String password = in.readLine();
        try {
            userManager.login(username, password);
            sendCommand(false, "Login successful");
            return true;
        } catch (LoginException e) {
            sendCommand(false, "Login failed. Username or password is incorrect.");
            return false;
        }
    }

    private boolean register() throws IOException {
        String name;
        while (true) {
            sendCommand(true, "Enter your name: ");
            name = in.readLine();
            if (userManager.isNameAvailable(name)) {
                break;
            }
            sendCommand(false, name + " is occupied. Pick another one. ");
        }
        sendCommand(true, "Enter your password: ");
        String password = in.readLine();
        boolean registered = userManager.register(name, password);
        sendCommand(false, "Registration " + (registered ? "successful" : "failed"));
        return registered;
    }

    private boolean mainMenu() throws IOException {
        boolean admin = userManager.isLoggedAdmin();
        String body = "Choose command:\n" +
                "1 - User (delete/logout)\n" +
                "2 - Message (send/read)\n" +
                "3 - Help (uptime/info/help)" +
                (admin ? "\n4 - Admin" : "");
        sendCommand(true, body);
        switch (in.readLine()) {
            case "1":
                userMenu();
                break;
            case "2":
                messageMenu();
                break;
            case "3":
                helpMenu();
                break;
            case "4":
                if (admin) {
                    return adminMenu();
                } else {
                    sendCommand(false, "Unknown command");
                    break;
                }
            default:
                sendCommand(false, "Unknown command");
        }
        return false;
    }

    private void userMenu() throws IOException {
        String body = "Choose command:\n" +
                "1 - Delete account\n" +
                "2 - Logout\n" +
                "3 - Back";
        while (true) {
            sendCommand(true, body);
            switch (in.readLine()) {
                case "1":
                    deleteAccount();
                    return;
                case "2":
                    logout();
                    return;
                case "3":
                    return;
                default:
                    sendCommand(false, "Unknown command");
            }
        }
    }

    private void deleteAccount() {
        userManager.deleteUser();
        sendCommand(false, "Account has been deleted");
    }

    private void logout() {
        userManager.logout();
        sendCommand(false, "Logout successful");
    }

    private void messageMenu() throws IOException {
        boolean run = true;
        String body = "Choose command:\n" +
                "1 - Send message\n" +
                "2 - Read your message\n" +
                "3 - Back";
        while (run) {
            sendCommand(true, body);
            switch (in.readLine()) {
                case "1":
                    sendMessage();
                    break;
                case "2":
                    readMessage();
                    break;
                case "3":
                    run = false;
                    break;
                default:
                    sendCommand(false, "Unknown command");
            }
        }
    }

    private void sendMessage() throws IOException {
        sendCommand(true, "Enter the name of the person you want to send a message to:");
        String username = in.readLine();
        sendCommand(true, "Enter your message:");
        String message = in.readLine();
        try {
            userManager.sendMessage(username, message);
            sendCommand(false, "Sending the message successful.");
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void readMessage() {
        try {
            Message message = userManager.readMessage();
            sendCommand(false, new Gson().toJson(message));
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void helpMenu() throws IOException {
        boolean run = true;
        String body = "Choose command:\n" +
                "1 - Show uptime\n" +
                "2 - Show info\n" +
                "3 - Show help\n" +
                "4 - Back";
        while (run) {
            sendCommand(true, body);
            switch (in.readLine()) {
                case "1":
                    showUptime();
                    break;
                case "2":
                    showInfo();
                    break;
                case "3":
                    showHelp();
                    break;
                case "4":
                    run = false;
                    break;
                default:
                    sendCommand(false, "Unknown command");
            }
        }
    }

    private void showUptime() {
        UpTime upTime = server.getUpTime();
        String json = new Gson().toJson(upTime);
        sendCommand(false, json);
    }

    private void showInfo() {
        JsonObject response = new JsonObject();
        response.addProperty("serverVersion", Server.getServerVersion());
        response.addProperty("creationDate", server.getStartDate().toString());
        sendCommand(false, response.toString());
    }

    private void showHelp() {
        JsonObject response = new JsonObject();
        response.addProperty("uptime", "Returns the days, hours, minutes, and seconds since the server started.");
        response.addProperty("info", "Returns the server version and creation date.");
        response.addProperty("help", "Returns a list of available commands.");
        response.addProperty("stop", "Stops the server and the client");
        sendCommand(false, response.toString());
    }

    private boolean adminMenu() throws IOException {
        boolean run = true;
        String body = "Choose command:\n" +
                "1 - Read user's message\n" +
                "2 - Delete user's account\n" +
                "3 - Stop servers\n" +
                "4 - Back";
        while (run) {
            sendCommand(true, body);
            switch (in.readLine()) {
                case "1":
                    readMessageOthers();
                    break;
                case "2":
                    deleteUserAccount();
                    break;
                case "3":
                    stop();
                    return true;
                case "4":
                    run = false;
                    break;
                default:
                    sendCommand(false, "Unknown command");
            }
        }
        return false;
    }

    private void readMessageOthers() throws IOException {
        sendCommand(true, "Enter the name of the person whose message you want to read: ");
        String username = in.readLine();
        try {
            Message message = userManager.readMessageOthers(username);
            sendCommand(false, new Gson().toJson(message));
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void deleteUserAccount() throws IOException {
        sendCommand(true, "Enter the name of the person you want to delete: ");
        String username = in.readLine();
        try {
            userManager.deleteUser(username);
            sendCommand(false, "Account has been deleted");
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    public void stop() throws IOException {
        sendCommand(false, "Closing", true);
        userManager.logout();
        in.close();
        out.close();
        server.close();
    }
}
