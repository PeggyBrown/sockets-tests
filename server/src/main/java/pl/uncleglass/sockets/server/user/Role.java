package pl.uncleglass.sockets.server.user;

public enum Role {
    USER,   ADMIN;

    public static Role get(int i) {
        return i == 1 ? ADMIN : USER;
    }
}
