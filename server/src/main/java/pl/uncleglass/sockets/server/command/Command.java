package pl.uncleglass.sockets.server.command;

public class Command {
    private final boolean responseRequired;
    private final String body;
    private final boolean endConnection;

    public Command(boolean responseRequired, String body, boolean endConnection) {
        this.responseRequired = responseRequired;
        this.body = body;
        this.endConnection = endConnection;
    }

    public Command(boolean responseRequired, String body) {
        this.responseRequired = responseRequired;
        this.body = body;
        this.endConnection = false;
    }
}
