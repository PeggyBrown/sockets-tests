package pl.uncleglass.sockets.server.server;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;

public class ServerTime {
    private static final int SECONDS_IN_A_DAY = 86_400;
    private static final int SECONDS_IN_A_HOUR = 3600;
    private static final int SECONDS_IN_A_MINUTE = 60;
    private final TimeSupplier timeSupplier;
    private final Instant startTime;
    private final LocalDate startDate;

    public ServerTime(TimeSupplier timeSupplier) {
        this.timeSupplier = timeSupplier;
        startTime = this.timeSupplier.get();
        startDate = LocalDate.now();
    }

    public UpTime getUpTime() {
        long duration = calculateDurationInSeconds();
        int days = calculateFullDays(duration);
        int hours = calculateRemainingHoursInADay(duration);
        int minutes = calculateRemainingMinutesInAnHour(duration);
        int seconds = calculateRemainingSecondsInAMinute(duration);

        return new UpTime(days, hours, minutes, seconds);
    }

    private long calculateDurationInSeconds() {
        Duration between = Duration.between(startTime, timeSupplier.get());
        return between.getSeconds();
    }

    private int calculateFullDays(long durationInSeconds) {
        long days = durationInSeconds / SECONDS_IN_A_DAY;
        return (int) days;
    }

    private int calculateRemainingHoursInADay(long durationInSeconds) {
        long hours = (durationInSeconds % SECONDS_IN_A_DAY) / SECONDS_IN_A_HOUR;
        return (int) hours;
    }

    private int calculateRemainingMinutesInAnHour(long durationInSeconds) {
        long minutes = (durationInSeconds % SECONDS_IN_A_HOUR) / SECONDS_IN_A_MINUTE;
        return (int) minutes;
    }

    private int calculateRemainingSecondsInAMinute(long durationInSeconds) {
        long seconds = durationInSeconds % SECONDS_IN_A_MINUTE;
        return (int) seconds;
    }

    public LocalDate getStartDate() {
        return startDate;
    }
}
