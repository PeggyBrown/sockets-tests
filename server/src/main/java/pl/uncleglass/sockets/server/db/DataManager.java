package pl.uncleglass.sockets.server.db;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Result;
import org.jooq.impl.DSL;
import pl.uncleglass.sockets.server.user.Message;
import pl.uncleglass.sockets.server.user.Role;
import pl.uncleglass.sockets.server.user.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import static pl.uncleglass.sockets.server.db.Tables.MESSAGES;
import static pl.uncleglass.sockets.server.db.Tables.USERS;

public class DataManager {
    private static final String URL = "jdbc:sqlite:sockets.db";

    public static void createTables() {
        String sql1 = """
                    CREATE TABLE IF NOT EXISTS messages (
                    	id INTEGER PRIMARY KEY AUTOINCREMENT,
                    	sender INTEGER NULL,
                    	body text  NULL,
                    	"owner" INTEGER NULL
                    );
                """;
        String sql2 = """
                CREATE TABLE IF NOT EXISTS users (
                	id INTEGER PRIMARY KEY AUTOINCREMENT,
                	username text NOT NULL,
                	"password" text  NOT NULL,
                	"role" INTEGER NULL
                );
                            """;
        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {
            stmt.execute(sql1);
            stmt.execute(sql2);

        } catch (SQLException e) {
        }

    }

    public boolean saveUser(User user) {
        if (user.getId() == null) {
            try (Connection connection = DriverManager.getConnection(URL)) {
                int execute = DSL.using(connection)
                        .insertInto(USERS,
                                USERS.USERNAME, USERS.PASSWORD, USERS.ROLE)
                        .values(user.getName(), user.getPassword(), user.getRole().ordinal())
                        .execute();
                return execute > 0;
            } catch (SQLException e) {
                return false;
            }
        } else {
            Queue<Message> messages = user.getMessages();
            try (Connection connection = DriverManager.getConnection(URL)) {
                DSLContext create = DSL.using(connection);
                Result<Record4<Integer, Integer, Integer, String>> messageResult = create
                        .select(MESSAGES.ID, MESSAGES.OWNER, MESSAGES.SENDER, MESSAGES.BODY)
                        .from(MESSAGES)
                        .where(MESSAGES.OWNER.eq(user.getId()))
                        .orderBy(MESSAGES.ID)
                        .fetch();

                LinkedList<Message> dbMessages = messageResult.stream()
                        .map(record -> new Message(
                                record.component1(),
                                record.component2(),
                                record.component3(),
                                record.component4())
                        )
                        .collect(Collectors.toCollection(LinkedList::new));

                dbMessages.forEach(
                        message -> {
                            if (!messages.contains(message)) {
                                create.delete(MESSAGES)
                                        .where(MESSAGES.ID.eq(message.getId()))
                                        .execute();
                            }
                        }
                );

                messages.forEach(
                        message -> {
                            if (message.getId() == null) {
                                create.insertInto(MESSAGES,
                                        MESSAGES.OWNER, MESSAGES.SENDER, MESSAGES.BODY)
                                        .values(message.getOwner(), message.getSender(), message.getBody())
                                        .execute();
                            }
                        }
                );
                return true;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
    }

    public List<String> getUsersList() {
        try (Connection connection = DriverManager.getConnection(URL)) {
            Result<Record1<String>> result = DSL.using(connection)
                    .select(USERS.USERNAME)
                    .from(USERS)
                    .fetch();
            return result.stream()
                    .map(record -> record.get(0, String.class))
                    .collect(Collectors.toList());
        } catch (SQLException e) {
            return new ArrayList<>();
        }
    }

    public User loadUser(String username) {
        try (Connection connection = DriverManager.getConnection(URL)) {
            DSLContext create = DSL.using(connection);
            Result<Record4<Integer, String, String, Integer>> userResult = create
                    .select(USERS.ID, USERS.USERNAME, USERS.PASSWORD, USERS.ROLE)
                    .from(USERS)
                    .where(USERS.USERNAME.eq(username))
                    .fetch();

            Record4<Integer, String, String, Integer> recordUser = userResult.get(0);
            User user = new User(
                    recordUser.component1(),
                    recordUser.component2(),
                    recordUser.component3(),
                    Role.get(recordUser.component4())
            );

            Result<Record4<Integer, Integer, Integer, String>> messageResult = create
                    .select(MESSAGES.ID, MESSAGES.OWNER, MESSAGES.SENDER, MESSAGES.BODY)
                    .from(MESSAGES)
                    .where(MESSAGES.OWNER.eq(user.getId()))
                    .orderBy(MESSAGES.ID)
                    .fetch();

            LinkedList<Message> messages = messageResult.stream()
                    .map(record -> new Message(
                            record.component1(),
                            record.component2(),
                            record.component3(),
                            record.component4())
                    )
                    .collect(Collectors.toCollection(LinkedList::new));

            user.setMessages(messages);
            return user;
        } catch (SQLException e) {
            return null;
        }
    }

    public void deleteUser(String username) {
        User user = loadUser(username);

        try (Connection connection = DriverManager.getConnection(URL)) {
            DSLContext create = DSL.using(connection);

            create.delete(MESSAGES)
                    .where(MESSAGES.OWNER.eq(user.getId()))
                    .execute();

            create.delete(USERS)
                    .where(USERS.ID.eq(user.getId()))
                    .execute();
        } catch (SQLException e) {
        }
    }
}