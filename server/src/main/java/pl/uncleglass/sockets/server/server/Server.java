package pl.uncleglass.sockets.server.server;

import pl.uncleglass.sockets.server.command.CommandManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;

public class Server {
    private static final String SERVER_VERSION = "0.1";
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private ServerTime serverTime;

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        serverTime = new ServerTime(new TimeSupplier());
        System.out.println("Server has started and listening to on port " + port);

        clientSocket = serverSocket.accept();
        CommandManager commandManager = new CommandManager(this);
        commandManager.run();
    }

    public static String getServerVersion() {
        return SERVER_VERSION;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public UpTime getUpTime() {
        return serverTime.getUpTime();
    }

    public LocalDate getStartDate() {
        return serverTime.getStartDate();
    }

    public void close() throws IOException {
        clientSocket.close();
        serverSocket.close();
    }
}
