package pl.uncleglass.sockets.server;

import pl.uncleglass.sockets.server.db.DataManager;
import pl.uncleglass.sockets.server.server.Server;

import java.io.IOException;

public class ServerApp {
    public static void main(String[] args) throws IOException {
        DataManager.createTables();
        Server server = new Server();
        server.start(6666);
    }
}
