package pl.uncleglass.sockets.server.user;

import java.util.Objects;

public class Message {
    private static final int MESSAGE_MAX_LENGTH = 255;
    private Integer id;
    private Integer owner;
    private final Integer sender;
    private final String body;

    public Message(Integer sender,Integer owner, String body) {
        this.sender = sender;
        this.owner = owner;
        if (body.length() > MESSAGE_MAX_LENGTH) {
            throw new IllegalArgumentException("Message is too long. Max length is " + MESSAGE_MAX_LENGTH + " characters");
        }
        this.body = body;
    }

    public Message(Integer id, Integer owner, Integer sender, String body) {
        this.id = id;
        this.owner = owner;
        this.sender = sender;
        this.body = body;
    }

    public Integer getOwner() {
        return owner;
    }

    public Integer getSender() {
        return sender;
    }

    public String getBody() {
        return body;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(id, message.id)
                && Objects.equals(owner, message.owner)
                && Objects.equals(sender, message.sender)
                && Objects.equals(body, message.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, owner, sender, body);
    }
}
