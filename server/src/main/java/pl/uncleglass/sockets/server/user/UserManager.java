package pl.uncleglass.sockets.server.user;

import pl.uncleglass.sockets.server.db.DataManager;

import javax.security.auth.login.LoginException;
import java.util.List;

public class UserManager {
    private User loggedUser;
    private final DataManager dataManager;

    public UserManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public boolean isNoUserLoggedIn() {
        return loggedUser == null;
    }

    public boolean register(String name, String password) {
        User user = new User(name, password, Role.USER);
        boolean saved = dataManager.saveUser(user);
        if (saved) {
            loggedUser = dataManager.loadUser(name);
        }
        return saved;
    }

    public boolean isNameAvailable(String name) {
        return !isUserRegistered(name);
    }

    public boolean isUserRegistered(String username) {
        List<String> usersList = dataManager.getUsersList();
        return usersList.contains(username);
    }

    public void login(String username, String password) throws LoginException {
        if (isUserRegistered(username)) {
            User user = dataManager.loadUser(username);
            if (password.equals(user.getPassword())) {
                loggedUser = user;
            } else {
                throw new LoginException();
            }
        } else {
            throw new LoginException();
        }
    }

    public void logout() {
        dataManager.saveUser(loggedUser);
        loggedUser = null;
    }

    public boolean isLoggedAdmin() {
        return loggedUser.isAdmin();
    }

    public void sendMessage(String username, String body) {
        if (!isUserRegistered(username)) {
            throw new IllegalArgumentException(username + " is not registered");
        }
        User user = dataManager.loadUser(username);
        Message message = new Message(loggedUser.getId(), user.getId(), body);
        user.addMessage(message);
        dataManager.saveUser(user);
    }

    public Message readMessage() {
        Message message = loggedUser.getMessage();
        if (message == null) {
            throw new IllegalStateException("No messages to read");
        }
        return message;
    }

    public Message readMessageOthers(String username) {
        if (!loggedUser.isAdmin()) {
            throw new IllegalStateException("You are not admin!");
        }
        if (!isUserRegistered(username)) {
            throw new IllegalArgumentException(username + " is not registered");
        }
        User user = dataManager.loadUser(username);
        Message message = user.getMessage();
        if (message == null) {
            throw new IllegalStateException("No messages to read");
        }
        dataManager.saveUser(user);
        return message;
    }

    public void deleteUser() {
        String username = loggedUser.getName();
        logout();
        dataManager.deleteUser(username);
    }

    public void deleteUser(String username) {
        if (!loggedUser.isAdmin()) {
            throw new IllegalStateException("You are not admin!");
        }
        if (!isUserRegistered(username)) {
            throw new IllegalArgumentException(username + " is not registered");
        }
        dataManager.deleteUser(username);
    }
}
