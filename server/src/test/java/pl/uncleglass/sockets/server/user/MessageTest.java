package pl.uncleglass.sockets.server.user;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;

class MessageTest {
    @Test
    @DisplayName("Should throw exception when message max length is exceeded")
    void shouldThrowExceptionWhenMessageMaxLengthIsExceeded() {
        String longMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
                " ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris" +
                " nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit " +
                "esse cillum dolore eu fugiat nulla pariatur.";

        assertThatIllegalArgumentException()
                .isThrownBy(() -> new Message(1, 2, longMessage));
    }

    @Test
    @DisplayName("Should not throw exception when message max length is not exceeded")
    void shouldNotThrowExceptionWhenMessageMaxLengthIsNotExceeded() {
        assertThatCode(() -> new Message(1, 2, "message"))
                .doesNotThrowAnyException();
    }
}