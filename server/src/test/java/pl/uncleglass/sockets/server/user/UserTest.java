package pl.uncleglass.sockets.server.user;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class UserTest {

    @Test
    @DisplayName("Should return true when user is admin")
    void shouldReturnTrueWhenUserIsAdmin() {
        User user = new User("test user", "pass", Role.ADMIN);

        boolean admin = user.isAdmin();

        assertThat(admin).isTrue();
    }

    @Test
    @DisplayName("Should return false when user is not admin")
    void shouldReturnFalseWhenUserIsNotAdmin() {
        User user = new User("test user", "pass", Role.USER);

        boolean admin = user.isAdmin();

        assertThat(admin).isFalse();
    }

    @Test
    @DisplayName("Should allow more than five messages to be added when the user is an admin")
    void shouldAllowMoreThanFiveMessagesToBeAddedWhenTheUserIsAnAdmin() {
        User admin = new User("admin", "pass", Role.ADMIN);
        admin.addMessage(new Message(1, 2, "First message"));
        admin.addMessage(new Message(1, 2, "Second message"));
        admin.addMessage(new Message(1, 2, "Third message"));
        admin.addMessage(new Message(1, 2, "Fourth message"));
        admin.addMessage(new Message(1, 2, "Fifth message"));

        assertThatCode(() -> admin.addMessage(new Message(1, 2, "Sixth message")))
                .doesNotThrowAnyException();
    }

    @Test
    @DisplayName("Should not allow more than five messages to be added when the user is not an admin")
    void shouldNotAllowMoreThanFiveMessagesToBeAddedWhenTheUserIsNotAnAdmin() {
        User user = new User("user", "pass", Role.USER);
        user.addMessage(new Message(1, 2, "First message"));
        user.addMessage(new Message(1, 2, "Second message"));
        user.addMessage(new Message(1, 2, "Third message"));
        user.addMessage(new Message(1, 2, "Fourth message"));
        user.addMessage(new Message(1, 2, "Fifth message"));

        assertThatIllegalStateException()
                .isThrownBy(() -> user.addMessage(new Message(1, 2, "Sixth message")));
    }

    @Test
    @DisplayName("Should allow adding messages when the user is not an admin and there are less that five messages")
    void shouldAllowAddingMessagesWhenTheUserIsNotAnAdminAndThereAreLessThatFiveMessages() {
        User user = new User("user", "pass", Role.USER);
        user.addMessage(new Message(1, 2, "First message"));
        user.addMessage(new Message(1, 2, "Second message"));
        user.addMessage(new Message(1, 2, "Third message"));

        assertThatCode(() -> user.addMessage(new Message(1, 2, "Fourth message")))
                .doesNotThrowAnyException();
    }
}