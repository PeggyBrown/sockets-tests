package pl.uncleglass.sockets.server.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import pl.uncleglass.sockets.server.db.DataManager;

import javax.security.auth.login.LoginException;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserManagerTest {
    private UserManager userManager;
    private final DataManager dataManager = mock(DataManager.class);

    @BeforeEach
    void setUp() {
        userManager = new UserManager(dataManager);
    }

    @Test
    @DisplayName("Should return true when class is created")
    void shouldReturnTrueWhenClassIsCreated() {
        boolean userLoggedIn = userManager.isNoUserLoggedIn();

        assertThat(userLoggedIn).isTrue();
    }

    @Test
    @DisplayName("Should return false when user is registered")
    void shouldReturnFalseWhenUserIsRegistered() {
        when(dataManager.saveUser(any(User.class))).thenReturn(true);
        userManager.register("user", "pass");

        boolean userLoggedIn = userManager.isNoUserLoggedIn();

        assertThat(userLoggedIn).isFalse();
    }

    @Test
    @DisplayName("Should return false when user is logged")
    void shouldReturnFalseWhenUserIsLogged() throws LoginException {
        when(dataManager.getUsersList()).thenReturn(Collections.singletonList("user"));
        when(dataManager.loadUser(anyString())).thenReturn(new User("user", "pass", Role.USER));
        userManager.login("user", "pass");

        boolean userLoggedIn = userManager.isNoUserLoggedIn();

        assertThat(userLoggedIn).isFalse();
    }

    @Test
    @DisplayName("Should return true when registration succeeds")
    void shouldReturnTrueWhenRegistrationSucceeds() {
        when(dataManager.saveUser(any(User.class))).thenReturn(true);

        boolean registered = userManager.register("user", "pass");

        assertThat(registered).isTrue();
        assertThat(userManager.isNoUserLoggedIn()).isFalse();
    }

    @Test
    @DisplayName("Should return false when registration failed")
    void shouldReturnFalseWhenRegistrationFailed() {
        when(dataManager.saveUser(any(User.class))).thenReturn(false);

        boolean registered = userManager.register("user", "pass");

        assertThat(registered).isFalse();
        assertThat(userManager.isNoUserLoggedIn()).isTrue();

    }

    @Test
    @DisplayName("Should return true when name is available")
    void shouldReturnTrueWhenNameIsAvailable() {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart"));

        boolean available = userManager.isNameAvailable("user");

        assertThat(available).isTrue();
    }

    @Test
    @DisplayName("Should return false when name is not available")
    void shouldReturnFalseWhenNameIsNotAvailable() {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart", "user"));

        boolean available = userManager.isNameAvailable("user");

        assertThat(available).isFalse();
    }

    @Test
    @DisplayName("Should throw exception when user is not registered")
    void shouldThrowExceptionWhenUserIsNotRegistered() {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart"));

        assertThatExceptionOfType(LoginException.class)
                .isThrownBy(() -> userManager.login("user", "pass"));
    }

    @Test
    @DisplayName("Should throw exception when password does not match")
    void shouldThrowExceptionWhenPasswordDoesNotMatch() {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart", "user"));
        when(dataManager.loadUser(anyString())).thenReturn(new User("user", "pass", Role.USER));

        assertThatExceptionOfType(LoginException.class)
                .isThrownBy(() -> userManager.login("user", "password"));
    }

    @Test
    @DisplayName("Should allow to log in")
    void shouldAllowToLogIn() {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart", "user"));
        when(dataManager.loadUser(anyString())).thenReturn(new User("user", "pass", Role.USER));

        assertThatCode(() -> userManager.login("user", "pass"))
                .doesNotThrowAnyException();
        assertThat(userManager.isNoUserLoggedIn()).isFalse();
    }

    @Test
    @DisplayName("Should throw exception when sending message to unregistered user")
    void shouldThrowExceptionWhenSendingMessageToUnregisteredUser() {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart"));

        assertThatIllegalArgumentException()
                .isThrownBy(() -> userManager.sendMessage("user", "test message"));
    }

    @Test
    @DisplayName("Should allow to send message")
    void shouldAllowToSendMessage() throws LoginException {
        when(dataManager.getUsersList()).thenReturn(Arrays.asList("mark", "bart", "user"));
        when(dataManager.loadUser(anyString())).thenReturn(new User("bart", "pass", Role.USER));
        userManager.login("user", "pass");
        when(dataManager.loadUser(anyString())).thenReturn(new User("user", "pass", Role.USER));

        assertThatCode(() -> userManager.sendMessage("user", "test message"))
                .doesNotThrowAnyException();
        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
        verify(dataManager).saveUser(argument.capture());
        Message message = argument.getValue().getMessage();
        assertThat(message.getBody()).isEqualTo("test message");
        assertThat(message.getSender()).isEqualTo("bart");
    }

}