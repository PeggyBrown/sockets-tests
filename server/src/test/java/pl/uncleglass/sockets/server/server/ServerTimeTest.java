package pl.uncleglass.sockets.server.server;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServerTimeTest {
    private ServerTime serverTime;
    private final TimeSupplier timeSupplier = mock(TimeSupplier.class);

    @BeforeEach
    void setUp() {
        when(timeSupplier.get()).thenReturn(Instant.parse("2020-01-01T00:00:00.00Z"));
        serverTime = new ServerTime(timeSupplier);
    }

    @Test
    @DisplayName("Should return up time 34 seconds")
    void shouldReturnUpTime34Seconds() {
        when(timeSupplier.get()).thenReturn(Instant.parse("2020-01-01T00:00:34.00Z"));

        UpTime upTime = serverTime.getUpTime();

        SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(upTime.getSeconds()).as("seconds").isEqualTo(34);
        assertions.assertThat(upTime.getMinutes()).as("minutes").isEqualTo(0);
        assertions.assertThat(upTime.getHours()).as("hours").isEqualTo(0);
        assertions.assertThat(upTime.getDays()).as("days").isEqualTo(0);
        assertions.assertAll();
    }

    @Test
    @DisplayName("Should return up time 23 minutes and 34 seconds")
    void shouldReturnUpTime23MinutesAnd34Seconds() {
        when(timeSupplier.get()).thenReturn(Instant.parse("2020-01-01T00:23:34.00Z"));

        UpTime upTime = serverTime.getUpTime();

        SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(upTime.getSeconds()).as("seconds").isEqualTo(34);
        assertions.assertThat(upTime.getMinutes()).as("minutes").isEqualTo(23);
        assertions.assertThat(upTime.getHours()).as("hours").isEqualTo(0);
        assertions.assertThat(upTime.getDays()).as("days").isEqualTo(0);
        assertions.assertAll();
    }

    @Test
    @DisplayName("Should return up time 13 hours 23 minutes and 34 seconds")
    void shouldReturnUpTime13Hours23MinutesAnd34Seconds() {
        when(timeSupplier.get()).thenReturn(Instant.parse("2020-01-01T13:23:34.00Z"));

        UpTime upTime = serverTime.getUpTime();

        SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(upTime.getSeconds()).as("seconds").isEqualTo(34);
        assertions.assertThat(upTime.getMinutes()).as("minutes").isEqualTo(23);
        assertions.assertThat(upTime.getHours()).as("hours").isEqualTo(13);
        assertions.assertThat(upTime.getDays()).as("days").isEqualTo(0);
        assertions.assertAll();
    }

    @Test
    @DisplayName("Should return up time 31 days 13 hours 23 minutes and 34 seconds")
    void shouldReturnUpTime31Days13Hours23MinutesAnd34Seconds() {
        when(timeSupplier.get()).thenReturn(Instant.parse("2020-02-01T13:23:34.00Z"));

        UpTime upTime = serverTime.getUpTime();

        SoftAssertions assertions = new SoftAssertions();
        assertions.assertThat(upTime.getSeconds()).as("seconds").isEqualTo(34);
        assertions.assertThat(upTime.getMinutes()).as("minutes").isEqualTo(23);
        assertions.assertThat(upTime.getHours()).as("hours").isEqualTo(13);
        assertions.assertThat(upTime.getDays()).as("days").isEqualTo(31);
        assertions.assertAll();
    }

    @Test
    @DisplayName("Should return today's date")
    void shouldReturnTodaysDate() {
        LocalDate startDate = serverTime.getStartDate();

        assertThat(startDate).isEqualTo(LocalDate.now());
    }
}